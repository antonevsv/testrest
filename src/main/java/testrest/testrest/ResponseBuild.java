package testrest.testrest;

import java.util.ArrayList;
import java.util.List;

public class ResponseBuild {
	public static final String errorpage=new String("<html><head><title>REST</title></head><body><h1>WRONG</h1></body></html>");
	public static final int entperpage = 10;
	public static final String top="<html><head><title>REST</title></head><body><h1>REST service</h1><br><h2>Tables</h2>";
	public static final String bottom="</body></html>";
	public static List<Table> tables=new ArrayList<Table>();
public static String BuildHtmlResponse(int table, int page){
	String resp= top;
	for (int i = 0;i<tables.size();i++){
		if (i==table){resp+="<br>"+tables.get(i).tableName+" You are here";}
		else{resp +="<br><a href=\"../"+i+"/"+page+"\">"+tables.get(i).tableName+"</a>";}
	}
	resp+= "<br><h2>Content</h2>";
	for (int i = (page-1)*entperpage;i<Math.min(page*entperpage,tables.get(table).content.size());i++)
	{
		resp+="<br><div>"+tables.get(table).content.get(i).name+"<br>"+tables.get(table).content.get(i).color+"</div>";
	}
	resp+="<br><br>";
	int pagelim=(int) Math.ceil(tables.get(table).content.size()/entperpage);
	int curr;
	for (int i=0;i<6;i++){
		curr=page+i-3;
		if (curr == page){resp+= page;}
		else{
			if((curr<1)||(curr>pagelim)){}
			else{resp +="   <a href=\"../"+table+"/"+curr+"\">"+curr+"</a>";}
		}
	}
	
	resp +=bottom;
	return resp;}
}
