package testrest.testrest;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;




@MappedSuperclass
public class DataInstance {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", updatable = false, nullable = false)
	int id;
	@Column(name = "name")
	String name;
	@Column(name = "color")
	String color;
	public DataInstance(){}
	public DataInstance(String nm,String col){
		this.name=nm;
		
		this.color=col;
	}
	@Override
	public String toString(){
		return id+name+color;}
}
