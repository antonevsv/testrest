package testrest.testrest;

import java.math.BigDecimal;
import java.rmi.server.Operation;
import java.time.ZonedDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.ext.Provider;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.sun.jersey.api.model.AbstractResourceModelContext;
import com.sun.jersey.api.model.AbstractResourceModelListener;

@Provider
public class Listener implements AbstractResourceModelListener {
	static StandardServiceRegistry registry;
    static SessionFactory sessionFactory;
    public void onLoaded(AbstractResourceModelContext modelContext) {
        System.out.println("Initialising data structure");
    	//System.out.print(true);
    	//App.kek = new String("fock");
        try {               	   
              // Create registry
              registry = new StandardServiceRegistryBuilder().configure().build();
              // Create MetadataSources
              MetadataSources sources = new MetadataSources(registry);
              // Create Metadata
              Metadata metadata = sources.getMetadataBuilder().build();
              // Create SessionFactory
              sessionFactory = metadata.getSessionFactoryBuilder().build();
              //sessionFactory =new Configuration().configure().buildSessionFactory();
        } catch (Exception e) {
               e.printStackTrace();
                    if (registry != null) {
                        StandardServiceRegistryBuilder.destroy(registry);
                    }
                }
                
             loadAllData();
        }  
    public static void loadAllData(){
    	Table cthulhustable = new Table("Cthulhus");
    	ResponseBuild.tables.add(cthulhustable);
    	Transaction transaction = null;
    	try  {
    		Session session = sessionFactory.openSession();
            List <DataInstance> instances = session.createQuery("from Cthulhu").list();
            cthulhustable.content=instances;
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    	Table seal = new Table("Seals");
    	ResponseBuild.tables.add(seal);
    	transaction = null;
    	try  {
    		Session session = sessionFactory.openSession();
            List <DataInstance> instances = session.createQuery("from Seal").list();
            seal.content=instances;
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
            public static void shutdown() {

                if (registry != null) {
                    StandardServiceRegistryBuilder.destroy(registry);      
                }
            }
 }
