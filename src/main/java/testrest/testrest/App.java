package testrest.testrest;

import java.util.Random;

import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;


@Path("/hello")
public class App {
	 
	//public static String kek = new String("123");
	
	/*public App(){
		System.out.print(true);
		kek = new String("fock");
	}*/
	
	//yes, this generation method is retarded, so what
	@GET
    @Path("/gen")
	public String generatedata(){
		Random rand =new Random();
		Table tb;
		
			tb=ResponseBuild.tables.get(0);
			tb.Create(new Cthulhu(Float.toString(rand.nextFloat()),Float.toString(rand.nextFloat())));
			tb=ResponseBuild.tables.get(1);
			tb.Create(new Seal(Float.toString(rand.nextFloat()),Float.toString(rand.nextFloat())));
		
		return "generated";}
    @GET
    @Path("/{tb}/{pg}")
    public String helloWorld(@PathParam("pg") String pg,@PathParam("tb") String tb) {
    	try{
    	int table = Integer.parseInt(tb);
    	int page = Integer.parseInt(pg);
        return ResponseBuild.BuildHtmlResponse(table, page);}
    	catch(Exception e){
    		return ResponseBuild.errorpage;
    	}
    }
    @POST  
    @Path("/add")  
    public Response addUser(  
        @FormParam("tableid") int id,  
        @FormParam("name") String name,  
        @FormParam("color") String color) {  
    	if(id==0){ResponseBuild.tables.get(id).Create(new Cthulhu(name,color));}
    	if(id==1){ResponseBuild.tables.get(id).Create(new Seal(name,color));}
        return Response.status(200)  
            .entity(" Entry added successfuly!<br> Table Id: "+id+"<br> Name: " + name+"<br> Color: "+color)  
            .build();  
    }  
    @DELETE
    @Path("/{tb}/del")
    public String deleteWorld(@PathParam("tb") String tb,@FormParam("instid") int id) {
    	try{
        	int table = Integer.parseInt(tb);
        	
        	ResponseBuild.tables.get(table).Delete(ResponseBuild.tables.get(table).Retrieve(id));
		return "DELETED";}
    	catch(Exception e){
    		return ResponseBuild.errorpage;
    	}

    }
    @POST  
    @Path("/upd")  
    public Response updUser(  
        @FormParam("tableid") int tbid, 
        @FormParam("id") int id,
        @FormParam("name") String name,  
        @FormParam("color") String color) {  
    	ResponseBuild.tables.get(tbid).Retrieve(id).name=name;
    	ResponseBuild.tables.get(tbid).Retrieve(id).color=color;
    	ResponseBuild.tables.get(id).Update(ResponseBuild.tables.get(tbid).Retrieve(id));
    	
        return Response.status(200)  
            .entity(" Entry updated successfuly!<br> Table Id: "+tbid+"<br> Emtry Id: "+id+"<br> Name: " + name+"<br> Color: "+color)  
            .build();  
    }  
    
    }


