package testrest.testrest;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class Table {
public String tableName;
public List<DataInstance> content;
public Table(String name){
	tableName = name;
	content=new ArrayList<DataInstance>();
}
//its public but kay
public DataInstance Retrieve(int num){
	return content.get(num);}
public void Update(DataInstance ins){
	
	Transaction transaction = null;
    try  {
    	Session session = Listener.sessionFactory.openSession();
        // start a transaction
        transaction = session.beginTransaction();
        // save the student objects
        session.update(ins);
        
        
        transaction.commit();
    } catch (Exception e) {
        if (transaction != null) {
            transaction.rollback();
        }
        e.printStackTrace();
    }}
public void Delete(DataInstance ins){
	content.remove(ins);
	Transaction transaction = null;
    try  {
    	Session session = Listener.sessionFactory.openSession();
        // start a transaction
        transaction = session.beginTransaction();
        // save the student objects
        session.delete(ins);
        
        
        transaction.commit();
    } catch (Exception e) {
        if (transaction != null) {
            transaction.rollback();
        }
        e.printStackTrace();}
    }
public void Create(DataInstance ins){
	content.add(ins);
	Transaction transaction = null;
    try  {
    	Session session = Listener.sessionFactory.openSession();
        // start a transaction
        transaction = session.beginTransaction();
        // save the student objects
        session.save(ins);
        
        
        transaction.commit();
    } catch (Exception e) {
        if (transaction != null) {
            transaction.rollback();
        }
        e.printStackTrace();
    }
}
}
